package com.starlabspring.springbootapp;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * ServerController
 */
@Controller
public class ServerController {
    @RequestMapping("/vision/getVerion")
    @ResponseBody
    String output(){
        return "<html><body><h1>Staples Vision Server v1.0</h1></body></html>" + "\n" + "<html><body><h1>powered by Spring!</h1></body></html>";
    }

}